# README #


Crowdjournal is an web application where users can add news, add comments about the news and then approve them or disapprove them ( I know that it sounds very similar to reddit)
It has a nice user interface, username is not used beside authentication to preserve anonymity.

You can at a glance view where the most controversy in the world:

![crowdjournal.JPG](https://bitbucket.org/repo/RgG9Xk/images/799910080-crowdjournal.JPG) 

When you are adding the news you can select the countries which are affected by that news and those countries change their color according to that

![crowdjournal2.JPG](https://bitbucket.org/repo/RgG9Xk/images/3833734848-crowdjournal2.JPG)

The application is done by using Django and Google Maps and of course lot of javascript, it puts an overlay on the google maps.