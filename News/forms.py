from django.forms import ModelForm
from django import forms
from News.models import News
from News.models import Argument
class NewsForm(ModelForm):
    class Meta:
        model=News
        fields=['title','description','url','locations']

class ArgumentForm(ModelForm):
    class Meta:
        model=Argument
        fields=['title','description','opinion']