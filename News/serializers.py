from django.forms import widgets
from rest_framework import serializers
from News.models import News, Argument,Location,Capital,Continent




class ArgumentSerializer(serializers.ModelSerializer):
    class Meta:
        model=Argument
        fields=('id','opinion','count','description','title')

class CapitalSerializer(serializers.ModelSerializer):
    class Meta:
        model=Capital
        fields=('latitude','longitude')


class NewsSerializer(serializers.ModelSerializer):
    argument_set=ArgumentSerializer(serializers.ModelSerializer);
    class Meta:
        model=News
        fields=('id','title','description','url','argument_set','yesCount','noCount','neutralCount','yesDividedByNo','discussionCount')


class LocationSerializer(serializers.ModelSerializer):
    news_set=NewsSerializer(serializers.ModelSerializer)
    capital=CapitalSerializer(serializers.ModelSerializer)
    class Meta:
        model=Location
        fields=('name','capital','news_set','yesCount','noCount','neutralCount','newsCount')


class LocationSerializerWithYesRankings(serializers.ModelSerializer):
    class Meta:
        model=Location
        fields=('name','yesCount','noCount','neutralCount','yesDividedByNo','newsCount')

class LocationSerializerWithNoRankings(serializers.ModelSerializer):
    class Meta:
        model=Location
        fields=('name','yesCount','noCount','neutralCount','yesDividedByNo','newsCount')
class LocationSerializerWithNeutralRankings(serializers.ModelSerializer):
    class Meta:

        model=Location
        fields=('name','yesCount','noCount','neutralCount','yesDividedByNo','newsCount')
class LocationSerializerWithNewsRankings(serializers.ModelSerializer):
    class Meta:
        model=Location
        fields=('name','yesCount','noCount','neutralCount','yesDividedByNo','newsCount')


class TitleNewsSerializer(serializers.ModelSerializer):
    class Meta:
        model=News
        fields=('title',)
class ContinentSerializer(serializers.ModelSerializer):
    class Meta:
        model=Continent
        fields=('name','yesCount','noCount','neutralCount','newsCount','discussionCount','yesDividedByNo')
class SingleContinentSerializer(serializers.ModelSerializer):
    location_set=LocationSerializer(serializers.ModelSerializer);
    capital=CapitalSerializer(serializers.ModelSerializer);
    class Meta:
        model=Continent
        fields=('name','yesCount','noCount','neutralCount','newsCount','discussionCount','location_set','capital','yesDividedByNo')
class ContinentNameSerializer(serializers.ModelSerializer):
    class Meta:
        model=Continent
        fields=('name',)

class LocationSerializerWithLessInfo(serializers.ModelSerializer):
    continent=ContinentNameSerializer(serializers.ModelSerializer)
    capital=CapitalSerializer(serializers.ModelSerializer)
    class Meta:
        model=Location
        fields=('name','capital','yesCount','noCount','neutralCount','newsCount','continent')


class LocationSerializerWithCoordinatesAndContinent(serializers.ModelSerializer):
    capital=CapitalSerializer(serializers.ModelSerializer)
    continent=ContinentSerializer(serializers.ModelSerializer)
    class Meta:
        model=Location
        fields=("capital","continent","name")

class ShortNewsSerializer(serializers.ModelSerializer):
    locations=LocationSerializerWithCoordinatesAndContinent(serializers.ModelSerializer)
    class Meta:
        model=News
        fields=("locations","url","title")








