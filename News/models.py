from django.db import models
from django.forms import ModelForm


# Create your models here.

OpinionTypes=(
('Y','Yes'),
('N','No'),
('E','Neutral'),)



class Location ( models.Model):
    name=models.CharField(max_length=150);
    capital=models.ForeignKey("Capital");
    yesCount=models.IntegerField(default=0)
    noCount=models.IntegerField(default=0)
    neutralCount=models.IntegerField(default=0)
    newsCount=models.IntegerField(default=0)
    continent=models.ForeignKey("Continent")
    yesDividedByNo=models.FloatField(default=0);
    discussionCount=models.IntegerField(default=0)
    def __unicode__(self):
        return  self.name + str(self.id);


#News.html
class News(models.Model):
    title = models.CharField(max_length=150)
    description= models.TextField(blank=True)
    yesCount=models.PositiveIntegerField(default=0)
    noCount=models.PositiveIntegerField(default=0)
    locations=models.ManyToManyField(Location)
    url=models.URLField(default="http://www.bbc.com/news/world-us-canada-27645569")
    neutralCount=models.PositiveIntegerField(default=0)
    argumentCount=models.PositiveIntegerField(default=0)
    discussionCount=models.IntegerField(default=0)
    yesDividedByNo=models.FloatField(default=0);



    def __unicode__(self):
        return self.title + str(self.pk)
    def getYesArguments(self):
        return self.argument_set.filter(opinion='Y')
    def getNoArguments(self):
        return self.argument_set.filter(opinion='N')
    def getNeutralArguments(self):
        return self.argument_set.filter(opinion='E')
#Arguments
class Argument (models.Model):
    news=models.ForeignKey("News")
    title = models.CharField(max_length=150)
    description = models.TextField()
    opinion = models.CharField(max_length=50,choices=OpinionTypes)
    count=models.PositiveIntegerField(default=1)
    def __unicode__(self):
        return self.title






class Capital(models.Model):
    latitude=models.FloatField();
    longitude=models.FloatField();
    name=models.CharField(max_length=150);
    def __unicode__(self):
        return self.name
class Continent(models.Model):
    capital=models.ForeignKey('Capital');
    name=models.CharField(max_length=150);
    yesCount=models.PositiveIntegerField(default=0);
    noCount=models.PositiveIntegerField(default=0);
    neutralCount=models.PositiveIntegerField(default=0);
    newsCount=models.PositiveIntegerField(default=0)
    discussionCount=models.PositiveIntegerField(default=0)
    yesDividedByNo=models.FloatField(default=0);
    def __unicode__(self):
        return self.name























