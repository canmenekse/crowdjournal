function getRankingOfTheCountryWithProperty(name, countryRankings, property)
{


    var currentRanking = 0;
    var ranking = 999;
    var prevCount;
    if (property == "countryyesrankings")
    {
        prevCount = countryRankings[0].yesCount;
    }
    else if (property == "countrynorankings")
    {
        prevCount = countryRankings[0].noCount;
    }
    else if (property == "countryneutralrankings")
    {
        prevCount = countryRankings[0].neutralCount;
    }
    else if (property=="countrynewsrankings")
    {
        prevCount=countryRankings[0].newsCount
    }
    else
    {
        console.log("issue");
    }


    $.each(countryRankings, function (i)
    {

        if (property == "countryyesrankings")
        {
            currentIndexCount = countryRankings[i].yesCount;
        }
        else if (property == "countrynorankings")
        {
            currentIndexCount = countryRankings[i].noCount;
        }
        else if (property == "countryneutralrankings")
        {
            currentIndexCount = countryRankings[i].neutralCount
        }
        else if(property=="countrynewsrankings")
        {
            currentIndexCount=countryRankings[i].newsCount
        }




        if (currentIndexCount != prevCount)
        {
            currentRanking++;
        }

        prevCount = currentIndexCount;

        if (countryRankings[i].name == name)
        {

            if (currentIndexCount != 0)
            {
                ranking = currentRanking;
            }
            else
            {
                ranking = -1;
            }

        }


    });
   // console.log(name + " Ranking " + property + " is " + ranking);
    return ranking;
}



function getActualRankingOfTheCountryWithProperty(name, countryRankings, property)
{


    var currentRanking = 0;
    var ranking=0;
    var currentIndexCount=0;

    if (property == "countryyesrankings")
    {
        prevCount = countryRankings[0].yesCount;
    }
    else if (property == "countrynorankings")
    {
        prevCount = countryRankings[0].noCount;
    }
    else if (property == "countryneutralrankings")
    {
        prevCount = countryRankings[0].neutralCount;
    }
    else if (property=="countrynewsrankings")
    {
        prevCount=countryRankings[0].newsCount
    }
    else
    {
        console.log("issue");
    }
    prevCount=0;


    $.each(countryRankings, function (i)
    {
        console.log('i:' + i + ", prevCount:" + prevCount + " , currentRanking : " + currentRanking+  ",  currentIndexCount" + currentIndexCount)

        if (property == "countryyesrankings")
        {
            currentIndexCount = countryRankings[i].yesCount;
        }
        else if (property == "countrynorankings")
        {
            currentIndexCount = countryRankings[i].noCount;
        }
        else if (property == "countryneutralrankings")
        {
            currentIndexCount = countryRankings[i].neutralCount
        }
        else if(property=="countrynewsrankings")
        {
            currentIndexCount=countryRankings[i].newsCount
        }
        else
        {
            console.log("Propert is different it is " + property)
        }





        if (currentIndexCount != prevCount && currentIndexCount!=0)
        {
            currentRanking++;
            console.log('Incremented because '+ currentIndexCount+ "!=" + currentRanking )
        }

        prevCount = currentIndexCount;

        if (countryRankings[i].name == name)
        {


                ranking = currentRanking;




        }


    });
   //console.log(name + " Ranking " + property + " is " + ranking);
    if(ranking!=0)
    {
    return ranking-1;
    }
    return ranking;
}








function registerControl(controlDiv, map) {

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map
  controlDiv.style.padding = '5px';

  // Set CSS for the control border
  var controlUI = document.createElement('DIV');
controlUI.style.cursor = 'pointer';
controlUI.style.backgroundImage = "url(static/images/theme/icons/signup.png)";
controlUI.style.height = '28px';
controlUI.style.width = '70px';
controlUI.title = 'Click to set the map to Home';
  controlDiv.appendChild(controlUI);



  google.maps.event.addDomListener(controlUI, 'click', function() {
    showModalWithId('#registerModal');
  });
    google.maps.event.addDomListener(controlUI, 'mouseover', function() {
    controlUI.style.backgroundImage="url(static/images/theme/icons/signupHover.png)";
  });
    google.maps.event.addDomListener(controlUI, 'mouseout', function() {
    controlUI.style.backgroundImage="url(static/images/theme/icons/signup.png)";
  });

}


function logoControl(controlDiv, map) {

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map
  controlDiv.style.padding = '5px';

  // Set CSS for the control border
  var controlUI = document.createElement('DIV');
controlUI.style.cursor = 'default';
controlUI.style.backgroundImage = "url(static/images/theme/icons/logo.png)";
controlUI.style.height = '100px';
controlUI.style.width = '80px';
controlUI.title = 'Click to set the map to Home';
controlDiv.appendChild(controlUI);


}


function logoutControl(controlDiv,map)
{
     // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map
  controlDiv.style.padding = '5px';

  // Set CSS for the control border
  var controlUI = document.createElement('DIV');
controlUI.style.cursor = 'pointer';
controlUI.style.backgroundImage = "url(static/images/theme/icons/logout.png)";
controlUI.style.height = '25px';
controlUI.style.width = '25px';
controlUI.title = 'Click to set the map to Home';
  controlDiv.appendChild(controlUI);



  google.maps.event.addDomListener(controlUI, 'click', function() {
    window.location = '/logout'
  });
}






function globeControl(controlDiv, map) {

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map
  controlDiv.style.padding = '5px';

  // Set CSS for the control border
  var controlUI = document.createElement('DIV');
controlUI.style.cursor = 'pointer';
controlUI.style.backgroundImage = "url(static/images/theme/icons/globe.png)";
controlUI.style.height = '100px';
controlUI.style.width = '80px';
controlUI.title = 'Click to set the map to Home';
controlDiv.appendChild(controlUI);
    google.maps.event.addDomListener(controlUI, 'click', function() {
     lat=40.52
     lng=34.34;
     var myLatLng=new google.maps.LatLng(lat, lng)
     map.setZoom(2);
     map.setCenter(myLatLng);
  });


}



function usernameControl(controlDiv,map)
{

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map.
  controlDiv.style.padding = '5px';

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.cursor = 'default';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to set the map to Home';
    controlUI.style.marginTop='4px';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = "Raleway";
  controlText.style.fontSize = '10pt';

  controlText.style.paddingLeft = '4px';
  controlText.style.paddingRight = '4px';
  controlText.innerHTML = username;
  controlText.style.fontWeight=500;
  controlUI.appendChild(controlText);


}



function currentMapTypeControl(controlDiv,map)
{

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map.
  controlDiv.style.padding = '5px';

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.cursor = 'default';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to set the map to Home';
    controlUI.style.marginTop='4px';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
   ShowCurrentControlText = document.createElement('div');
  ShowCurrentControlText.style.fontFamily = "Raleway";
  ShowCurrentControlText.style.fontSize = '10pt';

  ShowCurrentControlText.style.paddingLeft = '4px';
  ShowCurrentControlText.style.paddingRight = '4px';
  ShowCurrentControlText.innerHTML = "Agreed";
  ShowCurrentControlText.style.fontWeight=500;
  controlUI.appendChild(ShowCurrentControlText);


}




function settingsControl(controlDiv,map)
{
    controlDiv.style.padding = '5px';

  // Set CSS for the control border
  var controlUI = document.createElement('DIV');
controlUI.style.cursor = 'pointer';
controlUI.style.backgroundImage = "url(static/images/theme/icons/settings.png)";
controlUI.style.height = '25px';
controlUI.style.width = '25px';
controlUI.title = 'Click to set the map to Home';
controlDiv.appendChild(controlUI);
    google.maps.event.addDomListener(controlUI, 'click', function() {
     showRefreshModal()
  });

}


function loginControl(controlDiv,map)
{
      controlDiv.style.padding = '5px';

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to set the map to Home';
  controlDiv.appendChild(controlUI);
  controlUI.style.marginTop='5px';
  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.fontFamily = "Raleway";
  controlText.style.fontSize = '10pt';


  controlText.style.paddingLeft = '4px';
  controlText.style.paddingRight = '4px';
  controlText.innerHTML = 'LOG IN';
  controlText.style.fontWeight=600;
  controlUI.appendChild(controlText);
    controlDiv.appendChild(controlUI);
    google.maps.event.addDomListener(controlUI, 'click', function() {
     showModalWithId('#loginModal');
  });
}


function controlsWhenLoggedIn()
{

    //Map Controls
        // var registerControlDiv = document.createElement('div');
        // var registerControlInstance = new registerControl(registerControlDiv, map);
        // registerControlDiv.index = 1;
        // map.controls[google.maps.ControlPosition.TOP_RIGHT].push(registerControlDiv);

         var logoDiv=document.createElement('div');
         var logoControlInstance=new logoControl(logoDiv,map);
         logoDiv.index=1
         map.controls[google.maps.ControlPosition.TOP_LEFT].push(logoDiv);

         var globeDiv=document.createElement('div');
         var globeControlInstance=new  globeControl(globeDiv,map);
         globeDiv.index=0
         map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(globeDiv);
         var logoutDiv=document.createElement('div');
         var logoutInstance=new logoutControl(logoutDiv,map);
        logoutDiv.index=1
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(logoutDiv);


         //
        var settingsDiv=document.createElement('div')
        var settingsInstance=new settingsControl(settingsDiv,map);
        settingsDiv.index=2
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(settingsDiv);
        //
        var usernameDiv=document.createElement('div')
        var usernameInstance=new usernameControl(usernameDiv,map);
        usernameDiv.index=3
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(usernameDiv);

      var showCurrentMapTypeDiv=document.createElement('div');
      var showCurrentMapTypeControlInstance=new currentMapTypeControl(showCurrentMapTypeDiv,map);
      showCurrentMapTypeDiv.index=1;
      map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(showCurrentMapTypeDiv)




}

function controlsWhenNotLoggedIn()
{

    //Map Controls
         var registerControlDiv = document.createElement('div');
         var registerControlInstance = new registerControl(registerControlDiv, map);
         registerControlDiv.index = 1;
         map.controls[google.maps.ControlPosition.TOP_RIGHT].push(registerControlDiv);

         var logoDiv=document.createElement('div');
         var logoControlInstance=new logoControl(logoDiv,map);
         logoDiv.index=1
         map.controls[google.maps.ControlPosition.TOP_LEFT].push(logoDiv);

         var globeDiv=document.createElement('div');
         var globeControlInstance=new  globeControl(globeDiv,map);
         globeDiv.index=0
         map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(globeDiv);


        var loginDiv=document.createElement('div');
        var loginControlInstance= new loginControl(loginDiv,map);
        loginDiv.index=2;
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(loginDiv);


      var showCurrentMapTypeDiv=document.createElement('div');
      var showCurrentMapTypeControlInstance=new currentMapTypeControl(showCurrentMapTypeDiv,map);
      showCurrentMapTypeDiv.index=1;
      map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(showCurrentMapTypeDiv)

}









function shadeColor(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;
    G = (G<255)?G:255;
    B = (B<255)?B:255;

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

    return "#"+RR+GG+BB;
}




Number.prototype.map = function ( in_min , in_max , out_min , out_max ) {
  return ( this - in_min ) * ( out_max - out_min ) / ( in_max - in_min ) + out_min;
}



function renderGlobe(data, countryInfos, map, name,property)
{
    console.log("Globe start");
    geo = data.features[0].geometry;

    $.each(geo.coordinates, function (i)
    {
        lats = [];
        longs = [];
        coordinates = geo.coordinates[i][0];
        polygonCoordinates = createPolygonFromSinglePolygonCoordinates(coordinates, lats, longs);



        receivedCountryInfoData = countryInfos[0][0];

        capital = receivedCountryInfoData.capital;
        latitude = capital.latitude;
        longitude = capital.longitude;


        ranking=getRankingOfTheCountryWithProperty(name,receivedCountryRankings,property)

        polygonColor =globeColor;
        createPolygon(latitude, longitude, name, polygonColor, map, polygonCoordinates)
    });


    console.log("Globe end")
    geo = null;
}

function getPolygonColor(ranking)
{

     /*
    if(ranking==0)
    {
       return "red"

    }
    else if (ranking == 1)
    {
       return "orange";
    }
    else if(ranking==2)
    {
        return "yellow"
    }
    else if(ranking==3)
    {
        return "green"
    }
   */
    if(ranking==-1)
    {
        return emptyColor;
    }
    else
    {


        //scaledRanking=ranking.map(0,181,0,100);
        scaledRanking=ranking
        console.log("Ranking is" + ranking +" and is mapped to " + scaledRanking);
        rankingColor=colorUtility.colourAt(scaledRanking)
        return rankingColor
        //return "purple";
    }




}

function zIndexOfPolygon(name)
{
    if(name!="Globe")
    {
        return 1;
    }
    return 0;

}

function nth_occurrence (string, char, nth) {
    var first_index = string.indexOf(char);
    var length_up_to_first_index = first_index + 1;

    if (nth == 1) {
        return first_index;
    } else {
        var string_after_first_occurrence = string.slice(length_up_to_first_index);
        var next_occurrence = nth_occurrence(string_after_first_occurrence, char, nth - 1);

        if (next_occurrence === -1) {
            return -1;
        } else {
            return length_up_to_first_index + next_occurrence;
        }
    }
}


function getSummaryByUrl(id,url)
{
    console.log("called getSummaryByUrl")
    //customUrl="http://clipped.me/algorithm/clippedapi.php?url=http://www.bbc.com/news/world-asia-27435856";
   // var getUrl='http://clipped.me/algorithm/clippedapi.php?url='+url
    url="http://www.pedestrian.tv/news/arts-and-culture/the-daily-telegraph-goes-with-the-ferals-are-revol/94b004d6-c51f-4ba6-b854-32ccfa12f0cd.htm";
    getUrl="http://api.diffbot.com/v3/article?token=33bfa8334266532da6e819804b4fb766&paging=false&timeout=10000&url="+url +"&fields=title,text"
    $.getJSON(getUrl).success(function(data){
        console.log("Entered here");
        objectsField=data.objects;
        text=objectsField[0].text;
        title=objectsField[0].title;
        index=nth_occurrence(text,'.',3)
        shortText=text.substr(0,index+1)
        shortText+='<br/><div><a href="'+url+'">Original Source </a></div>';

        $('#summaryModalInfo').html(shortText);
        $('#summaryModal').modal('show');



    })


}











//Creates the polygon with given Options
function createPolygon(latitude, longitude, name, polygonColor, map, polygonCoordinates, createdPolygons)
{

    zIndex = zIndexOfPolygon(name);

    var polyToDraw = new google.maps.Polygon(
    {
        path: polygonCoordinates,
        strokeColor: strokeColor,
        strokeOpacity: 0.1,
        strokeWeight: strokeWeight,
        fillColor: polygonColor,
        fillOpacity: 1,
        zIndex: zIndex

    });

    polyToDraw.setMap(map);

    //Will be referenced from polygon
    var polygonInfo = {
        'name': name,
        'latitude': latitude,
        'longitude': longitude
    };

    polyToDraw.objInfo = polygonInfo;
    polygons.push(polyToDraw);
    google.maps.event.addListener(polyToDraw, 'click', function (event)
    {

        apiLink = "http://localhost:8000/apisample/" + polyToDraw.objInfo.name
        htmlText = "";

        $.getJSON(apiLink).success(function (data)
        {
            receivedData = data[0];
            //console.log(JSON.stringify(receivedData))
            countryName=receivedData.name.toUpperCase();
            countryYesCount=receivedData.yesCount;
            countryNoCount=receivedData.noCount;
            countryNeutralCount=receivedData.neutralCount;
            htmlText += '><ul style="list-style-type: none; margin-left:-10%;">'
            htmlText+='<li><span><span class="countryName">' + countryName +"</span>" +'<span style="margin-left:2%;"><span class="smallNearCountry1">Y:</span>'+'<span class="smallNearCountry2">'+countryYesCount + '</span><span class="smallNearCountry1">N:</span>'+'<span class="smallNearCountry2">'+countryNoCount+'</span><span class="smallNearCountry1">NE:' +'</span><span class="smallNearCountry2">'+countryNeutralCount+'</span></span></li>';
            news = receivedData.news_set;
            if (news != 0)
            {
                 //htmlText += '<ul style="list-style-type: none; margin-left:-10%;">'
                //console.log(JSON.stringify(news))
                $.each(news.reverse(), function (i)
                {
                    htmlText+='<br/>'
                    argument_yesCount=news[i].yesCount;
                    argument_noCount=news[i].noCount;
                    argument_neutralCount=news[i].neutralCount;
                    id = news[i].id
                    externalUrl = news[i].url

                    add='<span> <a href="#" data-toggle="modal" data-target="#argumentModal"><img src="/static/images/agreed.png" style="width: 50px;height:50px;margin-top:-42px;"/></a></span>'
                    //add_with_button='<button style="width:20px;" id="'+id+'"onclick="displayModal">Temp</button>';
                   add_argument_modal="";
                    view_summary_modal='<input name="submit" id="'  +id+ '"type="button" class="summaryInfoWindow" onclick="getSummaryByUrl('+id+','+'\''+externalUrl+'\')"tabindex="8"  />'
                    if(authenticated==true)
                   {
                    add_argument_modal='<input name="submit" id="'  +id+ '"type="button" class="argumentInfoWindow" onclick="displayAddArgumentModal('+id+')"tabindex="8"  />'
                   }

                   // yes_argument_modal='<input name="submit" id="'  +id+ '"type="button" style="width:20px;height:50px;margin-left:-4px;"onclick="displayYesArgumentModal('+id+')"tabindex="8"  />'
                    yes_argument_modal='<input name="submit" id="'  +id+ '"type="button" class="yesInfoWindow" onclick="createHtmlOfModalWithArgumentType('+'\'Y\''+','+id+','+'\'argumentAddYesIcon\''+','+'\'yes\''+')"tabindex="8"  />'
                    yes_count_print=argument_yesCount;
                    no_count_print=argument_noCount;
                    neutral_count_print=argument_neutralCount;
                    no_argument_modal='<input name="submit" id="'  +id+ '"type="button" class="noInfoWindow" onclick="createHtmlOfModalWithArgumentType('+'\'N\''+','+id+','+'\'argumentAddNoIcon\''+','+'\'no\''+')"tabindex="8"  />'
                    neutral_argument_modal='<input name="submit" id="'  +id+ '"type="button"  class="neutralInfoWindow" onclick="createHtmlOfModalWithArgumentType('+'\'E\''+','+id+','+'\'argumentAddNeutralIcon\''+','+'\'neutral\''+')"tabindex="8"  />'
                       // add_another='a';
                    htmlText = htmlText + '<li>'+'<div>' + news[i].title + '</div>' +yes_argument_modal+'<span class="infoBoxCounts">'+yes_count_print+'</span>'+no_argument_modal+'<span class="infoBoxCounts">'+no_count_print+'</span>'+neutral_argument_modal+'<span class="infoBoxCounts">'+neutral_count_print+'</span>'+add_argument_modal+view_summary_modal+ '</li>';
                });
                htmlText += "</ul>";
            }
            else
            {
                htmlText+="<p>" + "There is no news for this country " + "</p>"
            }
            var myLatlng = new google.maps.LatLng(polyToDraw.objInfo.latitude, polyToDraw.objInfo.longitude);

           // startOfDiv = '<div style="width:200px;">';
           startOfDiv='<div style="border:1px solid #666666;margin-top: 8px;background:#ebebeb;color:#666666;font-family:\'Myriad Pro\', Helvetica, sans-serif;font-size:11pt;padding: .5em 1em;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px;-webkit-box-shadow: 0 0  1px #666666;box-shadow: 0 0 1px #666666;"'
            endOfDiv = '</div>';
            //console.log(content)
            contentStr = startOfDiv + htmlText + endOfDiv;

           createInfoBox(contentStr,myLatlng);
            //Creating the infowndw
            /*
            var infoWindow = new google.maps.InfoWindow(
            {
                 content: contentStr
            });
            infoWindow.setPosition(myLatlng);
            infoWindow.open(map);
            infoWindows.push(infoWindow);
            */

            map.setZoom(5);
            map.setCenter(myLatlng);








            //createPin(myLatlng, map, polyToDraw.objInfo.name, htmlText);

         });
    });

    //tooltip to display country name
    //
           var marker = new MarkerWithLabel({
          position: new google.maps.LatLng(0,0),
          draggable: false,
          raiseOnDrag: false,
          map: map,
          labelContent: name,
          labelAnchor: new google.maps.Point(30, 20),
          labelClass: "labels", // the CSS class for the label
          labelStyle: {opacity: 1.0},
          icon: "http://placehold.it/1x1",
          visible: false
         });
         //
    //
    google.maps.event.addListener(polyToDraw,"mousemove",function(event){

            if(polyToDraw.objInfo.name!="Globe")
            {
                marker.setPosition(event.latLng);
                marker.setVisible(true);
            }
     });
    google.maps.event.addListener(polyToDraw, "mouseout", function(event) {
        marker.setVisible(false);
      });
}


google.maps.Map.prototype.clearOverlays = function ()
{
    for (var i = 0; i < markers.length; i++)
    {
        markers[i].setMap(null);

    }
    for (var i = 0; i < polygons.length; i++)
    {
        polygons[i].setMap(null)

    }
    polygons.length = 0;
    markers.length = 0;
}


function createInfoBox(content,myLatlng)
{
    var boxText=document.createElement("div");
    boxText.style.cssText="";
    boxText.innerHTML=content;
    var myOptions = {
		 content: boxText
		,disableAutoPan: false
		,maxWidth: 0
        ,minWidth:200
        ,
        pixelOffset: new google.maps.Size(-140, 0)

		,zIndex: null
		,boxStyle: {
		     background: "url('http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/examples/tipbox.gif') no-repeat",
            opacity: 1



		 }

		,closeBoxMargin: "10px 2px 2px 2px"
		,closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif"
		,infoBoxClearance: new google.maps.Size(1, 1)
		,isHidden: false
		,pane: "floatPane"
		,enableEventPropagation: false,
        contextmenu: true
	};

	var ib = new InfoBox(myOptions);
    ib.setPosition(myLatlng)
    ib.open(map)
    infoWindows.push(ib);




}



function createPin(coordinate, map, title, content)
{
    startOfDiv = '<div style="width:200px;">';
    endOfDiv = '</div>';
    console.log(content)
    contentStr = startOfDiv + content + endOfDiv;

    //Creating the infowndw
    var infowindow = new google.maps.InfoWindow(
    {
        content: contentStr
    });

    //Creating the mrkr
    var image_flag="/static/images/flag.png"
    var marker = new google.maps.Marker(
    {
        position: coordinate,
        map: map,
        title: title,
        icon:image_flag
    });
    markers.push(marker);
    infoWindows.push(infowindow);
    map.setZoom(5);
    map.setCenter(marker.getPosition());






    google.maps.event.addListener(marker, 'click', function ()
    {
        infowindow.open(map, marker);
    });

}


function getAllNecessaryInfo(name, map, polygonCoordinates,property)
{
    var countryApiLınk = "http://localhost:8000/countryshortinfo/" + name;
    var rankingOfCountriesApiLink = "http://localhost:8000/countryyesrankings";

    $.getJSON(countryApiLınk).success(function (countryInfos)
    {


        receivedCountryInfoData = countryInfos[0];
        if(receivedCountryInfoData==null)
        {
            console.log("the issue capital is" + name)
        }

        //console.log("Name: " +name)

        capital = receivedCountryInfoData.capital;

        latitude = capital.latitude;
        longitude = capital.longitude;


        //ranking = getRankingOfTheCountry(name, receivedCountryRankings);
        ranking=getRankingOfTheCountryWithProperty(name,receivedCountryRankings,property);
        polygonColor = getPolygonColor(ranking);
        createPolygon(latitude, longitude, name, polygonColor, map, polygonCoordinates)


    });
}





/*
// bounds of the desired area
var allowedBounds = new google.maps.LatLngBounds(
     new google.maps.LatLng(70.33956792419954, 178.01171875),
     new google.maps.LatLng(83.86483689701898, -88.033203125)
);
var lastValidCenter = map.getCenter();

google.maps.event.addListener(map, 'center_changed', function() {
    if (allowedBounds.contains(map.getCenter())) {
        // still within valid bounds, so save the last valid position
        lastValidCenter = map.getCenter();
        return;
    }

    // not valid anymore => return to last valid position
    map.panTo(lastValidCenter);
});
*/










 function customInitializeMap(property)
 {
      console.log("hi there");
     console.log("Latitude is " +userLatitude)
     console.log("hi there 2")

     zoomLevel=5;
     if(userLatitude==750 || zoomInToALocation==false)
     {
        userLatitude=40.52
        userLongitude=34.34;
        zoomLevel=1;
     }

        console.log("ZoomLevel is: "+ zoomLevel);
     // Map options
     var mapOptions = {
         center: new google.maps.LatLng(userLatitude, userLongitude),
         zoom: zoomLevel,
         streetViewControl: false,
         panControl: false,
         zoomControl: false,
         mapTypeControl:false,
         minZoom: 2,
         maxZoom: 15
     };
     //To avoid new initialization
     if(map==null)
     {
         map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
         //
         if(authenticated==false)
         {
          controlsWhenNotLoggedIn();
         }
         else if (authenticated==true)
         {
         controlsWhenLoggedIn();
         }
    }





     userLatitude=40.52
     userLongitude=34.34;
     zoomInToALocation=false;
     //Country related variables
     var polygonCoordinates;
     var name;
     var lats = [];
     var longs = [];
     var polygonsOfAllCountries;

     //Api Link
     var rankingLink = "http://localhost:8000/"+property;

     var jsonFileUrl="/static/json/countriesgeo.json";

     $.getJSON(jsonFileUrl).success(function (data)
     {

         name = data.features[0].properties.name
         var globeApiLink = "http://localhost:8000/countryshortinfo/" + name;
        // console.log("calling " + globeApiLink);

         $.when($.getJSON(globeApiLink), $.getJSON(rankingLink)).done(function (countryInfos, countryRankings)
         {

             receivedCountryRankings=countryRankings[0];
             lastInRankingName=receivedCountryRankings[receivedCountryRankings.length-1].name;
             console.log("That is " + lastInRankingName);
             upperBoundOfRanking=getActualRankingOfTheCountryWithProperty(lastInRankingName,receivedCountryRankings,property)

             if(upperBoundOfRanking==0)
             {
                 upperBoundOfRanking++;
             }


             colorUtility.setNumberRange(0,upperBoundOfRanking)
             console.log("upperBoundOfRanking is: " + upperBoundOfRanking )
            // console.log(JSON.stringify(receivedCountryRankings));

             renderGlobe(data,countryInfos, map, name,property);

             $.each(data.features, function (i)
             {

                 name = data.features[i].properties.name;
                 //We already processed the Globe data we do not want to do it again
                 if (name != "Globe")
                 {
                     //The polygons are stored into the geo
                     geo = data.features[i].geometry;

                     if (geo.type == "Polygon")
                     {

                         lats = [];
                         longs = [];
                         coordinates = geo.coordinates[0];
                         polygonCoordinates = createPolygonFromSinglePolygonCoordinates(coordinates, lats, longs);
                         getAllNecessaryInfo(name, map, polygonCoordinates,property);

                     }
                     else if (geo.type == "MultiPolygon")
                     {

                         $.each(geo.coordinates, function (i)
                         {

                             lats = [];
                             longs = [];
                             coordinates = geo.coordinates[i][0];
                             polygonCoordinates = createPolygonFromSinglePolygonCoordinates(coordinates, lats, longs);
                             //console.log(name);

                             getAllNecessaryInfo(name, map, polygonCoordinates,property);
                         });

                     }
                 }
             });

         });
     });


 }