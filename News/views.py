from django.shortcuts import render_to_response
from django.template import RequestContext
from django.templatetags.static import static
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from News.serializers import LocationSerializer,LocationSerializerWithLessInfo,LocationSerializerWithYesRankings,LocationSerializerWithNoRankings,LocationSerializerWithNeutralRankings,NewsSerializer,ArgumentSerializer,TitleNewsSerializer,ContinentSerializer,SingleContinentSerializer,ShortNewsSerializer;
from rest_framework.renderers import JSONRenderer


from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.forms import  UserCreationForm,AuthenticationForm
from News.forms import NewsForm,ArgumentForm
from News.models import News,Argument,Location,Capital,Continent
from pprint import pprint
import feedparser
import json
import urllib2

###############################################################
##Api methods
##############################################################

class JSONResponse(HttpResponse):
    def __init__(self,data, **kwargs):
        content=JSONRenderer().render(data)
        kwargs['content_type'] = "application/json"
        super(JSONResponse,self).__init__(content,**kwargs)

@csrf_exempt
def getNecessaryInfoToCustomPolygon(request,countryName):
    if(request.method=='GET'):
        country=Location.objects.filter(name=countryName)
        serializer=LocationSerializerWithLessInfo(country,many=True)
        return JSONResponse(serializer.data)
@csrf_exempt
def getYesRankingsByCountries(request):
    if(request.method=='GET'):
        country=Location.objects.all().order_by('-yesCount');
        serializer=LocationSerializerWithYesRankings(country,many=True)
        return JSONResponse(serializer.data)
def getNoRankingsByCountries(request):
    if(request.method=='GET'):
        country=Location.objects.all().order_by('-noCount');
        serializer=LocationSerializerWithNoRankings(country,many=True)
        return JSONResponse(serializer.data)
def getNeutralRankingsByCountries(request):
    if(request.method=='GET'):
        country=Location.objects.all().order_by('-neutralCount');
        serializer=LocationSerializerWithNeutralRankings(country,many=True)
        return JSONResponse(serializer.data);

def getNewsCountRankingsByCountries(request):
    if(request.method=='GET'):
        country=Location.objects.all().order_by('-newsCount');
        serializer=LocationSerializerWithNeutralRankings(country,many=True)
        return JSONResponse(serializer.data);


@csrf_exempt
def allNewsApi(request,countryName):
    if(request.method=='GET'):
        country=Location.objects.filter(name=countryName).order_by("-pk");
        serializer=LocationSerializer(country,many=True)
        return JSONResponse(serializer.data)
def getSingleNews(request,newsId):
    if(request.method=='GET'):
        news=News.objects.filter(pk=newsId)
        serializer=NewsSerializer(news,many=True)
        return JSONResponse(serializer.data)

def getArgumentsOfNewsWithOpinion(request,newsId,argumentOpinion):
    if(request.method=='GET'):
        print"the received Id is" + str(newsId);
        news=News.objects.get(pk=newsId)


        receivedArguments=news.argument_set
        sortedArguments=receivedArguments.all().filter(opinion=argumentOpinion).order_by('-count')
        serializer=ArgumentSerializer(sortedArguments,many=True)
        return JSONResponse(serializer.data)
def getNewsTitle(request,newsId):
     if(request.method=='GET'):
        print"the received Id is" + str(newsId);
        news=News.objects.filter(pk=newsId)
        serializer=TitleNewsSerializer(news,many=True);
        return JSONResponse(serializer.data)
def getYesRankingsByContinents(request):
    if(request.method=='GET'):
        continents=Continent.objects.all().order_by('-yesCount');
        serializer=ContinentSerializer(continents,many=True);
        return JSONResponse(serializer.data);

def getSingleContinent(request,continentName):
    if(request.method=='GET'):
        continent=Continent.objects.filter(name=continentName);
        serializer=SingleContinentSerializer(continent,many=True);
        return JSONResponse(serializer.data);
@csrf_exempt
def getNewsAndLocationsFromSingleContinent(request,continentName):
    if(request.method=='GET'):
        continentInstance=Continent.objects.get(name=continentName)
        locations=Location.objects.filter(continent=continentInstance)
        serializer=LocationSerializer(locations,many=True);
        return JSONResponse(serializer.data);
@csrf_exempt
def getLatestNews(request,countryName):
    if(request.method=='GET'):
        countryInstance=Location.objects.get(name=countryName)
        selectedNews=countryInstance.news_set.all();
        serializer=NewsSerializer(selectedNews);
        return JSONResponse(serializer.data);
def  getNoRankingsByContinents(request):
    if (request.method=='GET'):
        continents=Continent.objects.all().order_by('-noCount')
        serializer=ContinentSerializer(continents,many=True)
        return JSONResponse(serializer.data)
def getNeutralRankingsByContinents(request):
    if (request.method=='GET'):
        continents=Continent.objects.all().order_by('-neutralCount')
        serializer=ContinentSerializer(continents,many=True)
        return JSONResponse(serializer.data)
def getNewsRankingsByContinents(request):
    if (request.method=='GET'):
        continents=Continent.objects.all().order_by('-newsCount')
        serializer=ContinentSerializer(continents,many=True)
        return JSONResponse(serializer.data)
def getDiscussionRankingsByContinents(request):
    if (request.method=='GET'):
        continents=Continent.objects.all().order_by('-discussionCount')
        serializer=ContinentSerializer(continents,many=True)
        return JSONResponse(serializer.data)
def getCountryFromNews(request,newsId):
    if (request.method=="GET"):
        news=News.objects.filter(pk=newsId)
        serializer=ShortNewsSerializer(news,many=True)
        return JSONResponse(serializer.data)











###########################################################
##View Methods
##########################################################

def allNews(request):

    news=News.objects.all().order_by('title')
    context={'news':news}
    return render_to_response('News.html',context,context_instance=RequestContext(request))


def specificInspectView(request,newsId):
    #Get the related News
    selectedNews=News.objects.get(pk=newsId)
    context={'selectedNews':selectedNews}
    return render_to_response('inspectspecificnews.html',context,context_instance=RequestContext(request))

############################################################
##Add methods
############################################################
@csrf_exempt
def addNews(request):
    if request.user.is_authenticated():

        if request.method=='POST':

                form=NewsForm(request.POST)
                postedLocationId=request.POST['locations']
                newsTitle=request.POST['title']
                newsDescription=request.POST['description']
                newsUrl=request.POST['url']
                continents=[]
                newsCreated=False
                newspId=-1
                for locationId in request.POST.getlist('locations'):
                    addToList=True
                    location=Location.objects.get(pk=locationId)
                    location.newsCount=location.newsCount+1
                    continent=location.continent;
                    if continent.name in continents:
                        addToList=False
                    else:
                        addToList=True
                        continents.append(continent.name)
                        continent.newsCount+=1;
                        continent.save();
                    location.save()
                    capital=location.capital
                    latitude=capital.latitude
                    longitude=capital.longitude
                    request.session['latitude']=latitude;
                    request.session['longitude']=longitude;
                    #print ( "it is " + str(capital.name) + str(latitude) +"," + str(longitude))
                    if newsCreated==False:
                        new_news=News(title=newsTitle,description=newsDescription,url=newsUrl)
                        new_news.save();
                        new_news.locations.add(location)
                        newspId=new_news.pk
                        newsCreated=True
                    else:
                        new_news=News.objects.get(pk=newspId)
                        new_news.locations.add(location)
                    new_news.locations.add(location);
                    form=NewsForm(initial={})
                    form2=ArgumentForm();

                return HttpResponseRedirect('/mapwithdata10')
        else:
            form=NewsForm(initial={})
            context={'form':form}
            return render_to_response('formModel.html',context,context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/notloggedin')


###################################################################
##To Recreate database easily with sample datas
####################################################################
def createSampleData(request):
    capitalInstance=Capital(name='Paris',latitude=48.8567,longitude=2.3508)
    capitalInstance.save()
    locationInstance=Location(name='France',capital=capitalInstance)
    locationInstance.save()
    news=News()
    news.title='Paris anti pollution car ban '
    news.url='http://edition.cnn.com/2014/03/19/travel/paris-pollution-car-ban/index.html'
    news.description='pollution in Paris'
    news.save()
    news.locations.add(1);

    return render_to_response('createdsamplefiles.html')

def getTheOtherNameOfCountry(replacement,searched):
    if searched in replacement:
        return replacement[searched]
    else:
        return searched


def  createCountryDataFromInternalJSON(request):
    #url=static('json/countriesinfo.json')
    url='News/static/json/countriesinfo2.json'
    json_Response=open(url)
    count=0;
    countryData=json.load(json_Response)
    replacement= {'United States':'United States of America',
                  'Tanzania':'United Republic of Tanzania',
                  'Guinea-Bissau':'Guinea Bissau',
                  'Republic of Ireland': 'Ireland',
                  'Republic of Kosovo':'Kosovo',
                  'Republic of Macedonia':'Macedonia',
                  'Serbia':'Republic of Serbia',
                  'Palestine':'West Bank'
                };
    content=[]



    for country in countryData:
        count=count+1
        name=country['name']
        name=getTheOtherNameOfCountry(replacement,name)
        capital=country['capital']
        latlng=country['latlng']
        continent=country['region']
        if continent=="Americas":
            continent=country['subregion'];

        #print name
        #print latlng;
        if len(latlng)<2:
            latitude=-1
            longitude=-1
        else:



            latitude=latlng[0]
            #print latitude;
            longitude=latlng[1]
            #print longitude;
            if name!="" and capital!="":
                #check whether the capital already exists
                checkname=name
                checkCapital=Capital.objects.filter(name=capital)
                checkLocation=Location.objects.filter(name=checkname)



                checkContinent=Continent.objects.filter(name=continent)




                if checkCapital.exists():
                    print("We have " + str(capital))
                    previouslyCreated=1;
                else:
                    print("Entering here!")
                    capitalInstance=Capital(name=capital,latitude=latitude,longitude=longitude)
                    capitalInstance.save()

                if  checkLocation.exists():
                    print("We have " +str(name) )
                    previouslyCreated=1;


                else:


                    if checkContinent.exists():
                        continentInstance=Continent.objects.get(name=continent)
                        locationInstance=Location(name=name,capital=capitalInstance,continent=continentInstance)
                        locationInstance.save();

                    else:

                        #get Continent
                        capitalOfAContinent=getCapitalOfAContinent(continent);
                        capitalOfAContinent.save();
                        createdContinent=Continent(name=continent,capital=capitalOfAContinent);
                        createdContinent.save();
                        locationInstance=Location(name=name,capital=capitalInstance,continent=createdContinent);
                        locationInstance.save()




    return render_to_response('createdcountrydata.html')





def getCapitalOfAContinent(continent):

    if continent =="Europe":
        createdCapital=Capital(name="EuropeCapital",latitude=29,longitude=29);

    elif continent=="Asia":
        createdCapital=Capital(name="AsiaCapital",latitude=29,longitude=29);

    elif continent=="Oceania":
        createdCapital=Capital(name="OceniaCapital",latitude=29,longitude=29);

    elif continent=="Northern America":
        createdCapital=Capital(name="NorthernAmericaCapital",latitude=29,longitude=29);

    elif continent=="Southern America":
        createdCapital=Capital(name="SouthernAmericaCapital",latitude=29,longitude=29);
    elif continent=="Africa":
        createdCapital=Capital(name="AfricaCapital",latitude=29,longitude=29);
    elif continent=="N/A":
        createdCapital=Capital(name="NACapital",latitude=29,longitude=29);
    else:
        print continent;

    return createdCapital;


def  createCountryDataFromJSON(request):
    url="http://restcountries.eu/rest/v1/all"
    previouslyCreated=0;
    req=urllib2.Request(url)
    json_Response=urllib2.urlopen(req)
    countryData=json.load(json_Response)
    replacement= {'United States':'United States of America',
                  'Tanzania':'United Republic of Tanzania',
                  'Guinea-Bissau':'Guinea Bissau',
                  'Republic of Ireland': 'Ireland',
                  'Republic of Kosovo':'Kosovo',
                  'Republic of Macedonia':'Macedonia',
                  'Serbia':'Republic of Serbia',
                  'Palestine':'West Bank'
                };
    content=[]



    for country in countryData:
        name=country['name']
        name=getTheOtherNameOfCountry(replacement,name)
        capital=country['capital']
        latlng=country['latlng']
        print name
        print latlng;
        if len(latlng)<2:
            latitude=-1
            longitude=-1
        else:



            latitude=latlng[0]
            #print latitude;
            longitude=latlng[1]
            #print longitude;
            if name!="" and capital!="":
                capitalInstance=Capital(name=capital,latitude=latitude,longitude=longitude)
                capitalInstance.save()
                locationInstance=Location(name=name,capital=capitalInstance)
                locationInstance.save()
    addExtraCountriesAndCapitals()
    return render_to_response('createdcountrydata.html')



########################################################################
##Adding
########################################################################
@csrf_exempt
def addArguments(request,newsId):
    if request.user.is_authenticated():
        selectedNews=News.objects.get(pk=newsId)
        if request.method=='POST':
            form=ArgumentForm(request.POST)
            newForm=form.save(commit=False)
            newForm.news=selectedNews
            if newForm.opinion=='Y':
                selectedNews.yesCount+=1
            elif newForm.opinion=='N':
                selectedNews.noCount+=1
            elif newForm.opinion=='E':
                selectedNews.neutralCount+=1

            #Get all the locations related to that ( this should be improved)
            selectedNews.discussionCount+=1;
            selectedNews.yesDividedByNo=selectedNews.yesCount-selectedNews.noCount;
            locations = selectedNews.locations.all()
            continents=[];
            addToList=True
            for location in locations:
                continent=location.continent;
                addToList=True

                if continent.name in continents:
                    addToList=False
                else:
                    addToList=True
                    continents.append(continent.name)
                if newForm.opinion=='Y':
                    location.yesCount+=1
                    if addToList==True:
                        continent.yesCount+=1
                elif newForm.opinion=='N':
                    location.noCount+=1
                    if addToList==True:
                        continent.noCount+=1;
                elif newForm.opinion=='E':
                    location.neutralCount+=1
                    if addToList==True:
                        continent.neutralCount+=1;

                #if location.yesCount!=0 and location.noCount!=0:
                 #   location.yesDividedByNo=location.yesCount/location.noCount;
                #elif location.yesCount==0:
                 #   location.yesDividedByNo=1/location.noCount
                #elif location.noCount==0:
                 #   location.yesDividedByNo=location.yesCount/1;
                location.yesDividedByNo=location.yesCount-location.noCount;
                location.discussionCount+=1
                if addToList==True:
                    continent.discussionCount+=1
                    continent.yesDividedByNo=continent.yesCount-continent.noCount;
                    continent.save();
                location.save();





            selectedNews.argumentCount+=1
            selectedNews.save()
            newForm.save()
            return HttpResponseRedirect('/mapwithdata10')

        else:

            selectedNews=News.objects.get(pk=newsId)
            form=ArgumentForm()
            context={'selectedNews':selectedNews,'form':form}
            return render_to_response('specificnews.html',context,context_instance=RequestContext(request))

    else:
        return HttpResponseRedirect('/news')

def incrementArguments(request,argumentId):
    if request.user.is_authenticated():
        selectedArgument=Argument.objects.get(pk=argumentId)
        if request.method=='GET':
            selectedArgument.count=selectedArgument.count+1
            if selectedArgument.opinion=='Y':
                selectedArgument.news.yesCount=selectedArgument.news.yesCount+1
            elif selectedArgument.opinion=='N':
                selectedArgument.news.noCount=selectedArgument.news.noCount+1
            elif selectedArgument.opinion=='E':
                selectedArgument.news.neutralCount=selectedArgument.news.neutralCount+1

            #Add to the related Countries
            selectedArgument.news.discussionCount+=1
            selectedArgument.news.yesDividedByNo=selectedArgument.news.yesCount-selectedArgument.news.noCount
            locations = selectedArgument.news.locations.all()
            continents=[];

            for location in locations:
                addToList=True
                continent=location.continent;
                if continent.name in continents:
                    addToList=False
                else:
                    addToList=True
                    continents.append(continent.name)

                if selectedArgument.opinion=='Y':
                    location.yesCount+=1
                    if addToList==True:
                        continent.yesCount+=1
                elif selectedArgument.opinion=='N':
                    location.noCount+=1
                    if addToList==True:
                        continent.noCount+=1
                elif selectedArgument.opinion=='E':
                    location.neutralCount+=1
                    if addToList==True:
                        continent.neutralCount+=1


                location.yesDividedByNo=location.yesCount-location.noCount;
               # if location.yesCount!=0 and location.noCount!=0:
                #     location.yesDividedByNo=location.yesCount/location.noCount;
                #elif location.yesCount==0:
                 #    location.yesDividedByNo=1/location.noCount
                #elif location.noCount==0:
                 #    location.yesDividedByNo=location.yesCount/1;
                if addToList==True:
                    continent.discussionCount+=1
                    continent.yesDividedByNo=continent.yesCount-continent.noCount;
                    continent.save();
                location.discussionCount+=1;
                location.save()

            selectedArgument.news.save()
            selectedArgument.save()


    return HttpResponseRedirect('/mapwithdata10')
@csrf_exempt
def registrationView(request):
    #When the form is filled
    if request.method=='POST':
        user=UserCreationForm(request.POST)
        user.save()
        return HttpResponseRedirect('/mapwithdata10')

    else:
        registrationForm= UserCreationForm()
        context={'registrationForm':registrationForm}
        return render_to_response('registration.html',context,context_instance=RequestContext(request))
@csrf_exempt
def addExtraCountriesAndCapitals(request):


    #check all of them
    AsiaContinent=Continent.objects.get(name="Asia");
    AfricaContinent=Continent.objects.get(name="Africa");
    notAvailableContinent=Continent.objects.get(name="N/A");
    checkCapital=Capital.objects.filter(name="North Nicosia")
    checkLocation=Location.objects.filter(name="Northern Cyprus")
    if checkCapital.exists() or checkLocation.exists():
        print("They exist")
    else:
        capitalInstance=Capital(name='North Nicosia',latitude=35.33,longitude=33)
        capitalInstance.save()
        locationInstance=Location(name='Northern Cyprus', capital=capitalInstance,continent=AsiaContinent)
        locationInstance.save()

        capitalInstance=Capital(name='Hargeisa', latitude=10, longitude=44.5)
        capitalInstance.save()
        locationInstance=Location(name="Somaliland",capital=capitalInstance,continent=AfricaContinent)
        locationInstance.save()

        capitalInstance=Capital(name='None', latitude=0, longitude=0)
        capitalInstance.save()
        locationInstance=Location(name="Globe",capital=capitalInstance,continent=notAvailableContinent)
        locationInstance.capital=capitalInstance
        locationInstance.save()
    return HttpResponseRedirect('/mapwithdata10')

def mapView(request):

    form=NewsForm(initial={})
    form2=ArgumentForm();
    registrationForm= UserCreationForm()
    request.session['latitude']=750;
    request.session['longitude']=750;
    if 'authenticated' not in request.session:
        request.session['authenticated']='false'
    if 'refreshRate' not in request.session:
        request.session['refreshRate']=5000
    if 'mapType' not in request.session:
        request.session['mapType']="continentyesrankings"
    if'currentColorSet' not in request.session:
        request.session['currentColorMost']='#ED051C'
        request.session['currentColorLeast']='#F5EBEC'
        request.session['currentColorGlobe']='#EBEBEB'
        request.session['currentColorEmpty']='#c7c7c7'
        request.session['currentColorStroke']='#2F2F2F'
        request.session['currentWeightStroke']='1';
    context={'form':form,'form2':form2,'registrationForm':registrationForm};
    return render_to_response('mapWithData10.html',context,context_instance=RequestContext(request))


##################################################################################
##Authentication
##################################################################################
@csrf_exempt #ideally not
def loginView(request):
    #take a backup of current Request




    #a user should not be able to login again without being logged out
    if request.user.is_authenticated():
        return HttpResponseRedirect('/alreadyloggedin')

    else:
        #When the form is filled
        if request.method=='POST':
            username=request.POST['username']
            password=request.POST['password']
            user=authenticate(username=username,password=password)
            if user is not None:
                login(request,user)
                request.session['authenticated']='true'
                request.session['username']=username
                return HttpResponseRedirect('/mapwithdata10')
            else:
                return HttpResponseRedirect('/mapwithdata10')
        else:
            loginForm=AuthenticationForm()
            context={'loginForm':loginForm}
            return render_to_response('login.html',context,context_instance=RequestContext(request))


def logoutView(request):
    if request.user.is_authenticated():
        logout(request)
        return HttpResponseRedirect('/mapwithdata10')
    else:
        #User is not yet logged in
        return HttpResponseRedirect('/login')



###################################################################
##Other
###################################################################
def getNewsFromRSS(request):

    hdr = { 'User-Agent' : 'super happyaweawe flair bot by /u/spladug' }
    url="http://www.reddit.com/r/worldnews.json"
    req = urllib2.Request(url, headers=hdr)
    json_Response=urllib2.urlopen(req)
    data = json.load(json_Response)
        #pprint.pprint(data)
        #data2=data['data']['children'][3]['data']['url'];
    items = [ x['data'] for x in data['data']['children'] ]
    titlesAndLinks=dict()

    for singleNews in items:
            #singleItem=singleNews['data']
            singleUrl=singleNews['url']
            singleTitle=singleNews['title']
            print singleUrl
            singleTitle=singleTitle.encode('ascii', 'ignore')
            print singleTitle
            titlesAndLinks[singleTitle]=singleUrl

    context={'jsonData':titlesAndLinks}
    return render_to_response('viewFeeds.html',context,context_instance=RequestContext(request));
    ###################################################################
    ##Redirects
    ###################################################################
@csrf_exempt
def changeMapRefresh(request):
    form=NewsForm(initial={})
    form2=ArgumentForm();
    request.session['latitude']=750;
    request.session['longitude']=750;
    request.session['refreshRate']=request.POST['refreshInputName'];
    request.session['mapType']=request.POST['mapType'];

    return HttpResponseRedirect('/mapwithdata10')
    #return render_to_response('mapWithData9.html',context,context_instance=RequestContext(request))
def addSampleNewsData(request):
    i=1;
    #
    usaInstance="USA";
    usaTitleInstance= usaInstance + " news" +str(i);
    usaDescriptionInstance=usaTitleInstance+ " description"
    usaUrlInstance="http://www.google.com";
    usaNewsInstance=News(title=usaTitleInstance,description=usaDescriptionInstance,url=usaUrlInstance)
    usaNewsInstance.save()
    usaLocationInstance=Location.objects.get(name="United States of America")
    usaNewsInstance.locations.add(usaLocationInstance);
    usaLocationInstance.newsCount+=1;

    usaLocationInstance.save()
    usaNewsInstance.save()


    polandNameInstance="Poland";
    polandTitleInstance= polandNameInstance + "news" +str(i);
    polandDescriptionInstance=polandTitleInstance+ " description"
    polandUrlInstance="http://www.google.com";
    polandNewsInstance=News(title=polandTitleInstance,description=polandDescriptionInstance,url=polandUrlInstance)
    polandNewsInstance.save()
    polandLocationInstance=Location.objects.get(name="Poland")
    polandNewsInstance.locations.add(polandLocationInstance);
    polandLocationInstance.newsCount+=1;

    polandLocationInstance.save()
    polandNewsInstance.save()
   ##
    auNameInstance="Australia";
    auTitleInstance= auNameInstance + "news" +str(i);
    auDescriptionInstance=auTitleInstance+ " description"
    auUrlInstance="http://www.google.com";
    auNewsInstance=News(title=auTitleInstance,description=auDescriptionInstance,url=auUrlInstance)
    auNewsInstance.save()
    auLocationInstance=Location.objects.get(name="Australia")
    auNewsInstance.locations.add(auLocationInstance);
    auLocationInstance.newsCount+=1;

    auLocationInstance.save()
    auNewsInstance.save()
   ##
    saNameInstance="South Africa";
    saTitleInstance= saNameInstance + "news" +str(i);
    saDescriptionInstance=saTitleInstance+ " description"
    saUrlInstance="http://www.google.com";
    saNewsInstance=News(title=saTitleInstance,description=saDescriptionInstance,url=saUrlInstance)
    saNewsInstance.save()
    saLocationInstance=Location.objects.get(name="South Africa")
    saNewsInstance.locations.add(saLocationInstance);
    saLocationInstance.newsCount+=1;

    saLocationInstance.save()
    saNewsInstance.save()

    #Arguments


    usaArgument=Argument(opinion="N",count=3,title= " title No to " +usaTitleInstance,
                         description="No to description of" + usaTitleInstance,news=usaNewsInstance )
    usaArgument.save();
    usaNewsInstance.noCount+=3;
    usaLocationInstance.noCount+=3;
    usaArgument=Argument(opinion="E",count=3,title= " title Neutral to " +usaTitleInstance,
                         description="Neutral to description of" + usaTitleInstance,news=usaNewsInstance )
    usaArgument.save();
    usaNewsInstance.neutralCount+=1
    usaLocationInstance.neutralCount+=1


    usaNewsInstance.save()
    usaLocationInstance.save()

    #Poland News 1 Argument
    polandArgument=Argument(opinion="Y",count=1,title= " title Yes to " +polandTitleInstance,
                         description="Yes to description of" + polandTitleInstance,news=polandNewsInstance )
    polandArgument.save();
    polandNewsInstance.yesCount+=1;
    polandLocationInstance.yesCount+=1;
    polandArgument=Argument(opinion="N",count=1,title= " title No to " +polandTitleInstance,
                         description="No to description of" + polandTitleInstance,news=polandNewsInstance )
    polandArgument.save();
    polandNewsInstance.noCount+=1;
    polandLocationInstance.noCount+=1;



    polandLocationInstance.save()
    polandNewsInstance.save()

    #Australia News 1 Argument

    auArgument=Argument(opinion="Y",count=3,title= " title Yes to " +auTitleInstance,
                         description="Yes to description of" + auTitleInstance,news=auNewsInstance )
    auArgument.save();
    auNewsInstance.yesCount+=3
    auLocationInstance.yesCount+=3


    auArgument=Argument(opinion="Y",count=1,title= " title Yes_2 to " +auTitleInstance,
                         description="Yes_2 to description of" + auTitleInstance,news=auNewsInstance )
    auArgument.save();
    auNewsInstance.yesCount+=1
    auLocationInstance.yesCount+=1




    auArgument=Argument(opinion="N",count=1,title= " title No to " +auTitleInstance,
                         description="No to description of" + auTitleInstance,news=auNewsInstance )
    auArgument.save();
    auNewsInstance.noCount+=1;
    auLocationInstance.noCount+=1;
    auArgument=Argument(opinion="E",count=3,title= "Neutral to " +auTitleInstance,
                         description="Neutral to description of" + auTitleInstance,news=auNewsInstance )
    auArgument.save();
    auNewsInstance.neutralCount+=3
    auLocationInstance.neutralCount+=3


    auLocationInstance.save()
    auNewsInstance.save()

    #South Africa News 1 Argument

    saArgument=Argument(opinion="Y",count=3,title= " title Yes to " +saTitleInstance,
                         description="Yes to description of" + saTitleInstance,news=saNewsInstance )
    saArgument.save();
    saLocationInstance.yesCount+=3
    saNewsInstance.yesCount+=3

    saArgument=Argument(opinion="N",count=2,title= " title No to " +saTitleInstance,
                         description="No to description of" + saTitleInstance,news=saNewsInstance )
    saArgument.save();
    saLocationInstance.noCount+=2
    saNewsInstance.noCount+=2

    saArgument=Argument(opinion="E",count=4,title= " title Neutral to " +saTitleInstance,
                         description="Neutral to description of" + saTitleInstance,news=saNewsInstance )
    saArgument.save();
    saLocationInstance.neutralCount+=4
    saNewsInstance.neutralCount+=4

    saLocationInstance.save()
    saNewsInstance.save()


    #
    i=2

    usaInstance="USA";
    usaTitleInstance= usaInstance + " news" +str(i);
    usaDescriptionInstance=usaTitleInstance+ " description"
    usaUrlInstance="http://www.google.com";
    usaNewsInstance=News(title=usaTitleInstance,description=usaDescriptionInstance,url=usaUrlInstance)
    usaNewsInstance.save()
    usaLocationInstance=Location.objects.get(name="United States of America")
    usaNewsInstance.locations.add(usaLocationInstance);
    usaLocationInstance.newsCount+=1;

    usaLocationInstance.save()
    usaNewsInstance.save()


    polandNameInstance="Poland";
    polandTitleInstance= polandNameInstance + "news" +str(i);
    polandDescriptionInstance=polandTitleInstance+ " description"
    polandUrlInstance="http://www.google.com";
    polandNewsInstance=News(title=polandTitleInstance,description=polandDescriptionInstance,url=polandUrlInstance)
    polandNewsInstance.save()
    polandLocationInstance=Location.objects.get(name="Poland")
    polandNewsInstance.locations.add(polandLocationInstance);
    polandLocationInstance.newsCount+=1;

    polandLocationInstance.save()
    polandNewsInstance.save()
   ##
    auNameInstance="Australia";
    auTitleInstance= auNameInstance + "news" +str(i);
    auDescriptionInstance=auTitleInstance+ " description"
    auUrlInstance="http://www.google.com";
    auNewsInstance=News(title=auTitleInstance,description=auDescriptionInstance,url=auUrlInstance)
    auNewsInstance.save()
    auLocationInstance=Location.objects.get(name="Australia")

    auNewsInstance.locations.add(auLocationInstance);
    auLocationInstance.newsCount+=1;

    auLocationInstance.save()
    auNewsInstance.save()
   ##




    #Arguments when i is 2
    #Usa News 2 Arguments
    usaArgument=Argument(opinion="Y",count=2,title= " title Yes to " +usaTitleInstance,
                         description="Yes to description of" + usaTitleInstance,news=usaNewsInstance )
    usaArgument.save();
    usaNewsInstance.yesCount+=2;
    usaLocationInstance.yesCount+=2;


    usaNewsInstance.save()
    usaLocationInstance.save()

    #poland News 2 Arguments

    polandArgument=Argument(opinion="N",count=3,title= " title No to " +polandTitleInstance,
                         description="No to description of" + polandTitleInstance,news=polandNewsInstance )
    polandArgument.save();
    polandNewsInstance.noCount+=3
    polandLocationInstance.noCount+=3;
    polandArgument=Argument(opinion="E",count=2,title= " title Neutral to " +polandTitleInstance,
                         description="Neutral to description of" + polandTitleInstance,news=polandNewsInstance )
    polandArgument.save();
    polandNewsInstance.neutralCount+=2
    polandLocationInstance.neutralCount+=2

    polandNewsInstance.save()
    polandLocationInstance.save()


    #when i is 3
    i=3
    usaInstance="USA";
    usaTitleInstance= usaInstance + " news" +str(i);
    usaDescriptionInstance=usaTitleInstance+ " description"
    usaUrlInstance="http://www.google.com";
    usaNewsInstance=News(title=usaTitleInstance,description=usaDescriptionInstance,url=usaUrlInstance)
    usaNewsInstance.save()
    usaLocationInstance=Location.objects.get(name="United States of America")
    usaNewsInstance.locations.add(usaLocationInstance);
    usaLocationInstance.newsCount+=1;

    usaLocationInstance.save()
    usaNewsInstance.save()


    polandNameInstance="Poland";
    polandTitleInstance= polandNameInstance + "news" +str(i);
    polandDescriptionInstance=polandTitleInstance+ " description"
    polandUrlInstance="http://www.google.com";
    polandNewsInstance=News(title=polandTitleInstance,description=polandDescriptionInstance,url=polandUrlInstance)
    polandNewsInstance.save()
    polandLocationInstance=Location.objects.get(name="Poland")
    polandNewsInstance.locations.add(polandLocationInstance);
    polandLocationInstance.newsCount+=1;

    polandLocationInstance.save()
    polandNewsInstance.save()
   ##when i is 4
    i=4
    usaInstance="USA";
    usaTitleInstance= usaInstance + " news" +str(i);
    usaDescriptionInstance=usaTitleInstance+ " description"
    usaUrlInstance="http://www.google.com";
    usaNewsInstance=News(title=usaTitleInstance,description=usaDescriptionInstance,url=usaUrlInstance)
    usaNewsInstance.save()
    usaLocationInstance=Location.objects.get(name="United States of America")
    usaNewsInstance.locations.add(usaLocationInstance);
    usaLocationInstance.newsCount+=1;

    usaLocationInstance.save()
    usaNewsInstance.save()
    return HttpResponseRedirect('/mapwithdata10')

def sampleDataToCheckTheme(request):
    locations=Location.objects.all()
    i=0;
    for singleLocation in locations:
        singleLocation.yesCount=i;
        singleLocation.save()
        i=i+1
    return HttpResponseRedirect('/mapwithdata10')
@csrf_exempt
def colorofmap(request):
   if request.method=='POST':
    #pprint(request.POST)
    print("Selecteds are: ")
    most=request.POST['colorPickerMost'];
    least=request.POST['colorPickerLeast'];
    emptyColor=request.POST['colorPickerEmpty'];
    globeColor=request.POST['colorPickerGlobe'];
    strokeColor=request.POST['colorPickerStroke'];
    strokeWidth=request.POST['colorPickerStrokeWeight'];

    print(most+ "," +least + "," + emptyColor+ "," +globeColor + "," +strokeColor);
    request.session['currentColorStroke']=strokeColor
    request.session['currentColorMost']=most
    request.session['currentColorLeast']=least
    request.session['currentColorEmpty']=emptyColor
    request.session['currentColorGlobe']=globeColor
    request.session['currentColorSet']=1;
    request.session['currentWeightStroke']=strokeWidth;



    return  HttpResponseRedirect('/mapwithdata10')
















































