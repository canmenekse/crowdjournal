from django.contrib import admin
from News.models import News,Argument,Location,Capital,Continent
# Register your models here.

admin.site.register(News)
admin.site.register(Argument)
admin.site.register(Capital)
admin.site.register(Location)
admin.site.register(Continent)