from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from rest_framework import viewsets, routers
from News.models import News, Argument,Location,Capital


from django.contrib import admin
admin.autodiscover()



# ViewSets define the view behavior.
class LocationViewSet(viewsets.ModelViewSet):
    model = Location

class NewsViewSet(viewsets.ModelViewSet):
    model = News

class CapitalViewSet(viewsets.ModelViewSet):
    model = Capital


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'news', NewsViewSet)
router.register(r'locations', LocationViewSet)
router.register(r'capitals', CapitalViewSet)






urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'newsSiteProject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    (r'^news$','News.views.allNews'),
    (r'^addnews$','News.views.addNews'),
    (r'^thanks/?$',TemplateView.as_view(template_name='thanks.html')),
    (r'^newsaddargument/(?P<newsId>.*)/$','News.views.addArguments'),
    (r'^incrementargument/(?P<argumentId>.*)/$','News.views.incrementArguments'),
    (r'^register$','News.views.registrationView'),
    (r'^login$','News.views.loginView'),
    (r'^logout$','News.views.logoutView'),
    (r'^newsview/(?P<newsId>.*)/$','News.views.specificInspectView'),
    (r'^$','News.views.allNews'),
    (r'^alreadyloggedin/?$',TemplateView.as_view(template_name='alreadyLoggedIn.html')),
    (r'^notloggedin/?$',TemplateView.as_view(template_name='notloggedin.html')),
    (r'^getFeeds$','News.views.getNewsFromRSS'),
    (r'^api/', include(router.urls)),
    (r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    (r'^map$',TemplateView.as_view(template_name='map.html')),
    (r'^map2$',TemplateView.as_view(template_name='map2.html')),
    (r'^chart$',TemplateView.as_view(template_name='chart.html')),
    (r'^chart2$',TemplateView.as_view(template_name='chart2.html')),
    (r'^chart_2$',TemplateView.as_view(template_name='chart_2.html')),
    (r'^chart5$',TemplateView.as_view(template_name='chart5.html')),
    (r'^map7$',TemplateView.as_view(template_name='map7.html')),
    (r'^map9$',TemplateView.as_view(template_name='map9.html')),
    (r'^map12$',TemplateView.as_view(template_name='map12.html')),
    (r'^map15$',TemplateView.as_view(template_name='map15.html')),
    (r'^map19$',TemplateView.as_view(template_name='map19.html')),
    (r'^map22$',TemplateView.as_view(template_name='map22.html')),
    (r'^map23$',TemplateView.as_view(template_name='map23.html')),
    (r'^map25$',TemplateView.as_view(template_name='map25.html')),
    (r'^apisample/(?P<countryName>[a-zA-Z\ ]+)$','News.views.allNewsApi'),
    (r'^singlenews/(?P<newsId>[0-9]+)$','News.views.getSingleNews'),
    (r'^singlenewsopinion/(?P<newsId>[0-9]+)/(?P<argumentOpinion>[a-zA-z])$','News.views.getArgumentsOfNewsWithOpinion'),
    (r'^singlenewstitle/(?P<newsId>[0-9]+)$','News.views.getNewsTitle'),
    (r'^countryshortinfo/(?P<countryName>[a-zA-Z\ ]+)$','News.views.getNecessaryInfoToCustomPolygon'),
    (r'^countryyesrankings$','News.views.getYesRankingsByCountries'),
    (r'^countrynorankings$','News.views.getNoRankingsByCountries'),
    (r'^countryneutralrankings$','News.views.getNeutralRankingsByCountries'),

    (r'^countrynewsrankings$','News.views.getNewsCountRankingsByCountries'),
    (r'^maprefresh$','News.views.changeMapRefresh'),
    (r'^speechrecognition$',TemplateView.as_view(template_name='speechrecognition.html')),

    (r'^insertsampledata$','News.views.addSampleNewsData'),
    (r'^sampledatatochecktheme$','News.views.sampleDataToCheckTheme'),
    (r'^colorofmap$','News.views.colorofmap'),


    (r'^mapwithdata$',TemplateView.as_view(template_name='mapWithData.html')),
    (r'^createsampledata$','News.views.createSampleData'),
    (r'^createcountrydata$','News.views.createCountryDataFromJSON'),
    (r'^createextracapitalsandlocations$','News.views.addExtraCountriesAndCapitals'),
    (r'^angular$',TemplateView.as_view(template_name='Angular.html')),
    (r'^angularstrap$',TemplateView.as_view(template_name='AngularStrap.html')),
    (r'^modals$',TemplateView.as_view(template_name='modals.html')),
    (r'^createcountrydataInternal$','News.views.createCountryDataFromInternalJSON'),

    (r'^mapwithdataprev$',TemplateView.as_view(template_name='mapwithdataprev.html')),
    (r'^mapwithdata2$',TemplateView.as_view(template_name='mapWithData2.html')),
    (r'^mapwithdata7$',TemplateView.as_view(template_name='mapWithData7.html')),
    (r'^mapwithdata9$','News.views.mapView'),
    (r'^mapwithdata10$','News.views.mapView'),
    (r'^continentyesrankings$','News.views.getYesRankingsByContinents'),
    (r'^continentnorankings$','News.views.getNoRankingsByContinents'),
    (r'^continentneutralrankings$','News.views.getNeutralRankingsByContinents'),
    (r'^continentnewsrankings$','News.views.getNewsRankingsByContinents'),
    (r'^continentdiscussionrankings','News.views.getDiscussionRankingsByContinents'),
    (r'^getcountryfromnews/(?P<newsId>[0-9]+)$','News.views.getCountryFromNews'),



    (r'^singlecontinent/(?P<continentName>[a-zA-Z\ ]+)$','News.views.getSingleContinent'),
    (r'^getnewsandlocationsfromsinglecontinent/(?P<continentName>[a-zA-Z\ ]+)$','News.views.getNewsAndLocationsFromSingleContinent'),
    (r'^getthelatestnews/(?P<countryName>[a-zA-Z\ ]+)$','News.views.getLatestNews')
)
